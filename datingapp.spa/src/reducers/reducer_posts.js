import { FETCH_POSTS, NEW_POSTS } from '../actions/types';

export default function(state = {}, action) {
	switch (action.type) {
		case FETCH_POSTS:
			return {
				...state,
				items: action.payload,
			};
		default:
			return state;
	}
}
