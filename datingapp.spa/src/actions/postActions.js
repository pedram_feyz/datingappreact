import axios from 'axios';
import { FETCH_POSTS, NEW_POSTS } from './types';

export function fetchPosts() {
	return dispatch => {
		console.log();
		axios.get('https://jsonplaceholder.typicode.com/posts')
			//.then(res => res.json())
			.then(res => {
				console.log(res.data);
				dispatch({
					type: FETCH_POSTS,
					payload: res.data,
				});
			});
	};
}
// export const environment = {
// 	production: false,
// 	apiUrl: 'http://localhost:5000/api/',
//   };
// baesUrl = environment.apiUrl + 'auth/';
// post(this.baesUrl + 'login', model)

export function signin(values, callback) {
	return dispatch => {
		const request = axios.post('http://localhost:5000/api/auth/login', values);
		  request.then(function (res) {
			  const data = res.data;
			//console.log(response);
			callback(data);
			//return Promise.resolve(data);
		  })
	}
}