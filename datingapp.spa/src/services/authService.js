import decode from 'jwt-decode';

// export function loggedin(){
//     console.log('loggedin');
// }

export function saveTokenInLocalStorage(response) {
    console.log('saveInLocalStorage');
    console.log(response);
    const user = response;
    if (user) {
      // save the token that has sent from back-end in localStorage when loggin successed
      localStorage.setItem('token', user.token);
      localStorage.setItem('user', JSON.stringify(user.user));
     // this.decodedToken = decode(user.token);
    //  let currentUser = user.user;
    //   if (this.currentUser.photoUrl !== null) {
    //     this.changeMemberPhoto(this.currentUser.photoUrl);
    //   } else {
    //     this.changeMemberPhoto('../../assets/user.png');
    //   }
      // I did next line just for my knowlodge
      localStorage.setItem('decodedToken', JSON.stringify(decode(user.token)));
    }
}

function getToken() {
    // Retrieves the user token from localStorage
    return localStorage.getItem('token')
}

export function isTokenExpired(token) {
    try {
        const decoded = decode(token);
        if (decoded.exp < Date.now() / 1000) { // Checking if token is expired. N
            return true;
        }
        else
            return false;
    }
    catch (err) {
        return false;
    }
}

export function loggedin() {
    // Checks if there is a saved token and it's still valid
    const token = getToken() // GEtting token from localstorage
    return !!token && !isTokenExpired(token) // handwaiving here
}