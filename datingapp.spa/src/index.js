import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import { createStore, applyMiddleware } from 'redux';
import { BrowserRouter, Route } from 'react-router-dom';

import 'jquery/dist/jquery.min.js';
import 'bootstrap/dist/js/bootstrap.min.js';
import 'bootstrap/dist/css/bootstrap.min.css';
import './index.css';

import App from './App';
import Members from './components/members/Members'
import Lists from './components/lists/Lists';
import Messages from './components/messages/Messages';
import Signin from './components/signin/Signin';

import registerServiceWorker from './registerServiceWorker';

import store from './store';

ReactDOM.render(
    <Provider store={store}>
        <BrowserRouter>
            <App>
                <Route path="/" exact component={Signin} />
                <Route path="/members" exact component={Members} />
                <Route path="/lists" exact component={Lists} />
                <Route path="/messages" component={Messages} />
            </App>
        </BrowserRouter>
    </Provider>
    
    ,document.getElementById('root'));
registerServiceWorker();
