import React, { Component } from 'react';
import { Field, reduxForm } from 'redux-form';
import { connect } from 'react-redux';
import { signin } from '../../actions/postActions';
import { loggedin, saveTokenInLocalStorage } from '../../services/authService';

class Signin extends Component {

	renderField(field) {
		const { meta } = field; // field.meta
		const classWithDanger = `form-group col-md-4 ${meta.touched && meta.error ? 'has-danger' : ''}`;
		return (
			<div className={classWithDanger}>
				<label>{field.label}</label>
				<input className="form-control" type="text" {...field.input} />
				{/* onChange = {field.input.onChange} */}
				<div className="text-help">{meta.touched ? meta.error : ''}</div>
			</div>
		);
	}

	saveInLocalStorage = response => {
		saveTokenInLocalStorage(response);
		let bool = loggedin();
	}

	onSubmit = values => {
		this.props.signin(values, this.saveInLocalStorage);
		console.log(this.props);
		
	}
	render() {
		const { handleSubmit } = this.props;
		return (
			<form onSubmit={handleSubmit(this.onSubmit)}>
				<Field label="Email" name="username" component={this.renderField} />
				<Field label="Password" name="password" component={this.renderField} />
				<button type="submit" className="btn btn-primary">
					Submit
				</button>
			</form>
		);
	}

	// constructor(props) {
	// 	super(props);
	// 	this.state = {
	// 		email: '',
	// 		password: '',
	// 	};
    // }
    // onChange = (e) => {
	// 	console .log(e.target);

    //     this.setState({[e.target.name]: e.target.value});
    // }

    // validationForm = () => {}

    // onSubmit = (e) => {
    //     e.preventDefault();
    //     console.log(this.state);
    // }
	// render() {
	// 	if (localStorage.getItem('token')) {
	// 		return null;
	// 	}
	// 	return (
	// 		<div>
	// 			<form onSubmit={this.onSubmit}>
	// 				<div className="form-group col-md-4">
	// 					<label>Email address</label>
	// 					<input
	// 						type="email"
	// 						name="email"
    //                         value={this.state.email}
    //                         onChange={this.onChange}
	// 						className="form-control"
	// 						id="exampleInputEmail1"
	// 						aria-describedby="emailHelp"
    //                         placeholder="Enter email"
    //                         required
	// 					/>
	// 				</div>
	// 				<div className="form-group col-md-4">
	// 					<label >Password</label>
	// 					<input
	// 						type="password"
	// 						name="password"
    //                         value={this.state.password}
    //                         onChange={this.onChange}
	// 						className="form-control"
	// 						id="exampleInputPassword1"
	// 						placeholder="Password"
	// 						required
	// 					/>
	// 				</div>
	// 				<button type="submit" className="btn btn-primary">
	// 					Submit
	// 				</button>
	// 			</form>
	// 		</div>
	// 	);
	// }
}
function validate(values) {
	const errors = {};
	console.log('values');
	if (!values.username) {
		errors.username = 'Enter an email';
	}
	if (!values.password) {
		errors.password = 'Enter some password';
	}
	if (values.password && (values.password.length < 2 || values.password.length > 8)) {
		errors.password = 'password must be between 2 till 8 character';
	}

	// if errors is empty, the form is fine to submit
	return errors;
}

export default reduxForm({
	validate,
	form: 'PostsNewForm'
})(
	connect(null, { signin })(Signin)
);
// export default Signin;
