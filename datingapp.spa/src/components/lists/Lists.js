import React, { Component } from 'react';
import { connect } from 'react-redux';
import { fetchPosts } from '../../actions/postActions';

class Lists extends Component {
	post = () => {
		this.props.fetchPosts();
	};
	rendrPosts = () => {
        if(!this.props.posts){
            return null;
        }
		const postItems = this.props.posts.map(post => (
            <div key={post.id}>
              <h3>{post.title}</h3>
              <p>{post.body}</p>
            </div> 
          ));
        return postItems;
	};

	render() {
		return (
			<div>
				Lists
				<button onClick={this.post}>post</button>
				<div>{this.rendrPosts()}</div>
			</div>
		);
	}
}

function mapSateToProps(state) {
	console.log(state);
	return {
		posts: state.posts.items,
	};
}

export default connect(
	mapSateToProps,
	{ fetchPosts }
)(Lists);
