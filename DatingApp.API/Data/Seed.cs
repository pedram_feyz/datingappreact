using System.Collections.Generic;
using DatingApp.API.Models;
using Newtonsoft.Json;

namespace DatingApp.API.Data
{
    public class Seed
    {
        private readonly DataContext _context;

        public Seed(DataContext context)
        {
            _context = context;
        }

        public void SeedUsers(){
            // clean up user table with remove all user for the table
            //_context.Users.RemoveRange(_context.Users);
            //_context.SaveChanges();

            // seed users
            var userData = System.IO.File.ReadAllText("Data/UserDataSeed.json");
            var users = JsonConvert.DeserializeObject<List<User>>(userData);
            foreach (var user in users)
            {
                // create the password hash
                byte[] passwordHash, passwordSalt;
                // use this password "password" for all users
                CreatePasswordHash("password",out passwordHash, out passwordSalt);

                user.PasswordHash = passwordHash;
                user.PasswordSalt = passwordSalt;
                user.UserName = user.UserName.ToLower();
                
                _context.Users.Add(user);
            }
            
            _context.SaveChanges();
        }

        private void CreatePasswordHash(string password, out byte[] passwordHash, out byte[] passwordSalt)
        {
            // use using statement since HMACSHA512 is derived from IDisposable interface which means that when we Done with this method we can dispose this instance(clean the memory) 
            // and does not to need to wait for garbage collection
            using(var hmac = new System.Security.Cryptography.HMACSHA512())
            {
                passwordSalt = hmac.Key;
                passwordHash = hmac.ComputeHash(System.Text.Encoding.UTF8.GetBytes(password));
            }
        }
    }
}