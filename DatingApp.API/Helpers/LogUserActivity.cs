using System;
using System.Security.Claims;
using System.Threading.Tasks;
using DatingApp.API.Data;
using Microsoft.AspNetCore.Mvc.Filters;
// using Microsoft.Extensions.DependencyInjection;

namespace DatingApp.API.Helpers
{
    // use this class to track time of last activity of user
    // should be added to startup file
    // use it as attribute like it is done in userController
    public class LogUserActivity : IAsyncActionFilter
    {
        private readonly IDatingRepository _repo;
        public LogUserActivity(IDatingRepository repo)
        {
            _repo = repo;

        }
        public async Task OnActionExecutionAsync(ActionExecutingContext context, ActionExecutionDelegate next)
        {
            // this will run after the action is executed
            var resultContext = await next();

            var userId = int.Parse(resultContext.HttpContext.User.FindFirst(ClaimTypes.NameIdentifier).Value);
            // use Microsoft.Extensions.DependencyInjection nameSpace instead of proper dependency injection
            // since this is tiny task and we do not want spend so much time on it
            // var repo = resultContext.HttpContext.RequestServices.GetService<IDatingRepository>();
            // var user = await repo.GetUser(userId);

            // use proper dependency injection
            var user = await _repo.GetUser(userId);
            user.LastActive = DateTime.Now;
            await _repo.SaveAll();
        }
    }
}